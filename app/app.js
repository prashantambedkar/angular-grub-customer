var myApp = angular.module('example_codeenable', ['ui.router', 'ui.bootstrap']);

myApp.config(function ($stateProvider, $locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('/', {
            url: '/',
            templateUrl: 'templates/home.html',
            controller: 'home_contrloer',
            controllerAs: "home_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                        $rootScope.title = "ANGULARJS CODEGINITER MySQL CRUD";
                }]
            }
        })
        .state('/chefs', {
            url: '/chefs',
            templateUrl: 'templates/home1.html',
            controller: 'home1_contrloer',
            controllerAs: "home1_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                        $rootScope.title = "ANGULARJS CODEGINITER MySQL CRUD";
                }]
            }
        })   
        .state('/food-items', {
            url: '/food-items',
            templateUrl: 'templates/home2.html',
            controller: 'home2_contrloer',
            controllerAs: "home2_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                        $rootScope.title = "ANGULARJS CODEGINITER MySQL CRUD";
                }]
            }
        })   
        .state('/orders', {
            url: '/orders',
            templateUrl: 'templates/order.html',
            controller: 'order_contrloer',
            controllerAs: "order_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                        $rootScope.title = "ANGULARJS CODEGINITER MySQL CRUD";
                }]
            }
        })
        .state('/addresses', {
            url: '/addresses',
            templateUrl: 'templates/address.html',
            controller: 'address_contrloer',
            controllerAs: "address_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                        $rootScope.title = "ANGULARJS CODEGINITER MySQL CRUD";
                }]
            }
        })
        .state('/payment', {
            url: '/payment',
            templateUrl: 'templates/payment.html',
            controller: 'payment_contrloer',
            controllerAs: "payment_ctrl",
            resolve: {
                'title': ['$rootScope', function ($rootScope) {
                        $rootScope.title = "ANGULARJS CODEGINITER MySQL CRUD";
                }]
            }
        })
       
            
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });




});

